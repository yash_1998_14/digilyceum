angular.module('starter')
  .controller('CandidateDetailsCtrl', function ($scope, $state, $ionicHistory, $stateParams,AdminService ,UserService,IonicPopupService) {

    $scope.goBack = function () {
      $ionicHistory.clearCache().then(function () {
        $state.go('showdetails');
      });
    }
    $scope.$on('$ionicView.enter', function () {
      $scope.id = $stateParams.id;

      AdminService.getCandidateDetails($scope.id).$loaded().then(function(ref){
        $scope.data =ref;
      }).catch(function(error){
        console.log(error);
      })
    });
    $scope.verifyDetails =function(){
      UserService.setCrCandidatedata($scope.id,$scope.data).then(function(resp){
        AdminService.removeCandidateDetails($scope.id).then(function(resp){
          IonicPopupService.alert("Sucess","verify details success fully");
          $ionicHistory.clearCache().then(function () {
            $state.go('showdetails');
          });
        })
      })
    }

  });
