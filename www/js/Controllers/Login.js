angular.module('starter')
  .controller('LoginCtrl', function ($scope, $state, $ionicHistory, SessionService, IonicPopupService, sharedUtils, UserService, $http, RESOURCES,CommonService) {
    // $ionicPlatform.ready(function (SessionService, $ionicHistory, $state) {
    //   window.FirebasePlugin.setScreenName("Login Screen");
    // });
    //  $scope.device=SessionService.getDevice();

    if (window.FirebasePlugin) {
      window.FirebasePlugin.getToken(function (token) {
        // save this server-side and use it to push notifications to this device
        $scope.token = token;
        // alert(token);
      }, function (error) {
        console.error(error);
      });
    }

    $scope.$on('$ionicView.enter', function () {
      $scope.count = 0;
      $scope.subAdmin = false;
      $scope.enterOtp = false;
      $scope.user = {};
    });

    $scope.subAdminLogin = function () {
      $scope.count += 1;
      if ($scope.count == 7) {
        $scope.subAdmin = true;
      }
    }

    $scope.loginwithOtp = function (user) {
      sharedUtils.showLoading();
      UserService.getSubAdminObject(user.user_name).$loaded().then(function (ref) {
        $scope.subAdminData = ref;
        if (ref.length > 0) {
          $http.get(RESOURCES.URL + '/sendOtpForUserNameLogin?number=' + ref[0].phoneno)
            .then(function (response) {
              sharedUtils.hideLoading();
              $scope.enterOtp = true;
              $scope.subAdmin = false;
              $scope.myWelcome = response.data;
            });

        } else {
          IonicPopupService.alert("opps!", "User name is not available");
        }
      })

    }

    $scope.loginwithEmail = function (user) {
      sharedUtils.showLoading();
      if ($scope.validate(user) === false) {
        sharedUtils.hideLoading();
        return;
      }
      if (!!user) {
        UserService.getUserData(user.enrollment_number).$loaded().then(function (resp) {

          if (!!resp.email) {
            firebase.auth().signInWithEmailAndPassword(resp.email, user.password).then(function (result) {
              if (result.emailVerified == true) {
                let userObject = $scope.setUserObject(resp);
                SessionService.setUser(userObject);
                sharedUtils.hideLoading();
                $ionicHistory.clearCache().then(function () {
                  $state.go('tabsController.startPage');
                });
              }
              else {
                sharedUtils.hideLoading();
                IonicPopupService.alert("opps!", "Your Email is not verify");
              }

            }).catch(function (error) {
              sharedUtils.hideLoading();
              IonicPopupService.alert("opps!", error);
            });
          } else {
            sharedUtils.hideLoading();
            IonicPopupService.alert("opps!", "Invalid Data");
          }
        }).catch(function (error) {
          sharedUtils.hideLoading();
          IonicPopupService.alert("opps!", error);
        });
      }
      
    }


    $scope.setUserObject = function (Obj) {
      let userObject = {
        adhar_no: Obj.adhar_no,
        admission_year: Obj.admission_year,
        city: Obj.city,
        date_of_birth: Obj.date_of_birth,
        email: Obj.email,
        enrollment_number: Obj.enrollment_number,
        first_name: Obj.first_name,
        gender: Obj.gender,
        last_name: Obj.last_name,
        phone_no: Obj.phone_no,
        state: Obj.state,
        zip_code: Obj.zip_code,
        uid: Obj.uid,
        role: Obj.uid,
        token: $scope.token
      }
      return userObject;
    }

    $scope.goToSignUpPage = function () {
      $ionicHistory.clearCache().then(function () {
        $state.go('signup');
      });
    }

    $scope.verifyOtp = function (user) {
       
      if (!!user.code) {
        sharedUtils.showLoading();
        $http.get(RESOURCES.URL + '/verifyOtp?number=' + $scope.subAdminData[0].phoneno + "&otp=" + user.code)
          .then(function (response) {
            if (response.data == "OTP verified successfully") {
              sharedUtils.hideLoading();
              UserService.getUserData($scope.subAdminData[0].$id).$loaded().then(function (ref) {
                let data = subAdminObject(ref);
                SessionService.setUser(data);
                sharedUtils.hideLoading();
                $ionicHistory.clearCache().then(function () {
                  $state.go('tabsController.startPage');
                });
              })
              IonicPopupService.alert("Sucess", "OTP verified successfully");
            }
            else {
              sharedUtils.hideLoading();
              IonicPopupService.alert("Opps!", "OTP verification failed");
            }
          });
      }
      else {
        IonicPopupService.alert("Opps!", "Please eneter verification code");
      }

    }

    function subAdminObject(data) {
      let subAdObject = {
        branch: data.branch,
        email: data.email,
        faculty_name: data.faculty_name,
        phoneno: data.phoneno,
        uid: data.uid,
        username: data.username,
        name: data.name,
        role:data.role
      }
      return subAdObject;
    }

    $scope.validate = function(data) {
      if (!data) {
          IonicPopupService.alert("Oops!", "Please enter data.");
      } else if (CommonService.validateEmpty(data.enrollment_number, 'Oops!', 'Please enter enrollment number') === false) {
          return false;
      } else if (CommonService.validateEmpty(data.password, 'Oops!', 'Please enter password') === false) {
          return false;
      }
    }

  })
