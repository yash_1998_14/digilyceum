angular.module('starter')
  .controller('ShowDetailCtrl', function ($scope, $state, $ionicHistory, sharedUtils, SessionService, IonicPopupService, CommonService,
    UserService, ImageUploadService, $sce, AdminService) {


    $scope.goBack = function () {
      $ionicHistory.clearCache().then(function () {
        $state.go('tabsController.startPage');
      });
    }
    $scope.$on('$ionicView.enter', function () {
      UserService.getPendingList().$loaded().then(function (resp) {
        $scope.pendingList = resp;
      })
    })

    $scope.goToCandidatePage= function(id){
      $state.go('candidatedetails', {'id': id});
    }
  });
