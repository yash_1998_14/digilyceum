angular.module('starter')
  .controller('JoinElectionCtrl', function ($scope, $state, $ionicHistory, sharedUtils, SessionService, IonicPopupService, CommonService,
    UserService,ElectionService, $sce, FirebaseService, $ionicPopup) {
    // $ionicPlatform.ready(function (SessionService, $ionicHistory, $state) {
    //   window.FirebasePlugin.setScreenName("Join Screen");
    // });

    $scope.$on('$ionicView.enter', function () {
      $scope.data={};
      $scope.userObj = SessionService.getUser();
      $scope.data.number=$scope.userObj.enrollment_number;
    });

    $scope.goBack = function () {
      $ionicHistory.clearCache().then(function () {
        $state.go('tabsController.startPage');
      });
    }


    // $scope.uploadSheet=function(event){
    //   this.file = event.target.files[0];
    //   const fileReader = new FileReader();
    //   fileReader.onload = (e) => {
    //       this.arrayBuffer = fileReader.result;
    //       const data = new Uint8Array(this.arrayBuffer);
    //       const arr = new Array();
    //       data.forEach(d => {
    //           arr.push(String.fromCharCode(d));
    //       })
    //       const bstr = arr.join('');
    //       const workbook = XLSX.read(bstr, { type: 'binary' });
    //       const first_sheet_name = workbook.SheetNames[0];
    //       const worksheet = workbook.Sheets[first_sheet_name];
    //       const exceljson = XLSX.utils.sheet_to_json(worksheet, { raw: true });
    //       console.log(exceljson);
    //       exceljson.forEach(element => {
    //          let data={
    //            faculty_name:element.Name,
    //            email:element.Email,
    //            phoneno:element.PhoneNo,
    //            branch:element.Branch,
    //          }
    //          ElectionService.setFacultyData(data).$loaded().then(function(ref){})
    //       });

    //   }
    //   fileReader.readAsArrayBuffer(this.file);
    // }


    $scope.joinGame = function (data) {
      if ($scope.validate(data) === false) {
        return;
      }
      var myPopup = $ionicPopup.show({
        template: '<input type = "password" ng-model = "data.model">',
        title: 'Enter Password',
        scope: $scope,

        buttons: [
           { text: 'Cancel' }, {
              text: '<b>Submit</b>',
              type: 'button-positive',
              onTap: function(e) {

                 if (!$scope.data.model) {
                    //don't allow the user to close unless he enters model...
                    e.preventDefault();
                 } else {
                    return $scope.data.model;
                 }
              }
           }
        ]
     });

     myPopup.then(function(res) {
      sharedUtils.showLoading();
      firebase.auth().signInWithEmailAndPassword($scope.userObj.email, res).then(function (result) {
        UserService.getStudentData(data.number).$loaded().then(function (ref) {
          $scope.currentPin = FirebaseService.getFBObject('currentPinNumber');
          $scope.currentPin.$loaded().then(function (resp) {
            sharedUtils.hideLoading();
            if (resp.$value != data.game_code) {
              IonicPopupService.alert("Incorrect", "Game code is incorrect..");
            } else {
              $state.go("guestjoingame", {
                "PIN": data.game_code,
                "enrollment_number": data.number,
                "email":ref.email
              });
            }
          });
        });
      }).catch(function (error) {
          sharedUtils.hideLoading();
          IonicPopupService.alert("opps!", error);
      });

        // console.log('Tapped!', res);
     });
 }
    $scope.validate = function(data) {
      if (!data) {
          IonicPopupService.alert("Oops!", "Please enter data.");
      } else if (CommonService.validateEmpty(data.game_code, 'Oops!', 'Please enter game code') === false) {
          return false;
      } else if (CommonService.validateEmpty(data.number, 'Oops!', 'Please enter enrollment number') === false) {
          return false;
      }
    }
  });
