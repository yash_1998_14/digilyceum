angular.module('starter')
    .controller('AssignHostCtrl', function ($scope, $state, $ionicHistory, sharedUtils, RESOURCES, _, UserService, $http, IonicPopupService, SessionService) {


        $scope.$on('$ionicView.enter', function () {
            $scope.userObj = SessionService.getUser();
            $scope.selectFaculty = [];
            UserService.getFacultyData($scope.userObj.branch).$loaded().then(function (ref) {
                $scope.facultyData = ref;
            }).catch(function (error) {
                console.log(error);
            });
        })

        $scope.goBack = function () {
            $ionicHistory.clearCache().then(function () {
                $state.go('tabsController.startPage');
            });
        }

        $scope.sendUserName = function () {
            sharedUtils.showLoading();
            $scope.selectFaculty.forEach((element, index) => {
                if (element == true) {
                    var userName = $scope.facultyData[index].name + _.random(100, 999);
                    $scope.facultyData[index].username = userName;
                    let dataObject = subAdminObject($scope.facultyData[index]);
                    UserService.setSubAdminUserData(dataObject).$loaded().then(function (ref) {
                        dataObject.role = "host";
                        UserService.setUserData(ref.$id, dataObject).then(function (ref) {
                            $http.get(RESOURCES.URL + '/sendUserNameEmail?email=' + $scope.facultyData[index].email + "&username=" + userName)
                                .then(function (response) {
                                    $scope.myWelcome = response.data;
                                });
                        }).catch(function (error) {
                        });
                    }).catch(function (error) {
                    })
                }
            });
            IonicPopupService.alert("Sucess", "Username send sucessfully");
            sharedUtils.hideLoading();
        }

        function subAdminObject(data) {
            let subAdObject = {
                branch: data.branch,
                email: data.email,
                faculty_name: data.faculty_name,
                phoneno: data.phoneno,
                uid: data.uid,
                username: data.username,
                name: data.name
            }
            return subAdObject;
        }
    })