angular.module('starter')
  .controller('SignUpCtrl', function ($scope, $state, $ionicHistory, sharedUtils, SessionService, IonicPopupService, CommonService, sharedUtils, UserService, $ionicPlatform) {

    $scope.user = {};
    $scope.enrollment_number;
    $scope.goBack = function () {
      $ionicHistory.clearCache().then(function () {
        $state.go('login');
      });
    }
    $scope.device = SessionService.getDevice();
    if (window.FirebasePlugin) {
      window.FirebasePlugin.getToken(function (token) {
        // save this server-side and use it to push notifications to this device
        console.log(token);
        $scope.token = token;
      }, function (error) {
        console.error(error);
      });
    }
    $scope.getStudentData = function (enr) {
      UserService.getStudentData(enr).$loaded().then(function (ref) {
        $scope.user = ref;
      }).catch(function (error) { });
    }


    $scope.signwithEmail = function (user, enrollment_number) {
      sharedUtils.showLoading();
      if ($scope.validate(user) === false) {
        sharedUtils.hideLoading();
        return;
      }
      firebase.auth().createUserWithEmailAndPassword(user.email, user.password).then(function (result) {
        var userObj = {
          first_name: user.first_name,
          last_name: user.last_name,
          user_name: user.user_name,
          email: user.email,
          enrollment_number: enrollment_number,
          uid: result.uid,
          prevent: true,
          token: $scope.token,
        }
        UserService.setUserData(enrollment_number, userObj).then(function (ref) {
          sharedUtils.hideLoading();
          IonicPopupService.alert("Hold on", "Verification link send to your email");
          SessionService.setUser(userObj);
          let user = firebase.auth().currentUser;
          user.sendEmailVerification();
        }).catch(function (error) {
          sharedUtils.hideLoading();
          console.log(error);
        });
      }).catch(function (error) {
        console.log(error);
        sharedUtils.hideLoading();
        IonicPopupService.alert("opps!", error);
      });
    }

    $scope.validate = function (data) {
      if (!data) {
        IonicPopupService.alert("Oops!", "Please enter data.");
      } else if (CommonService.validateEmpty(data.enrollment_number, 'Oops!', 'Please enter enrollment number') === false) {
        return false;
      } else if (CommonService.validateEmpty(data.first_name, 'Oops!', 'Please enter firstname') === false) {
        return false;
      } else if (CommonService.validateEmpty(data.last_name, 'Oops!', 'Please enter lastname') === false) {
        return false;
      }else if (CommonService.validateEmpty(data.email, 'Oops!', 'Please enter Email') === false) {
        return false;
      }else if (CommonService.validateEmail(data.email, 'Oops!', 'Please enter valid Email') === false) {
        return false;
      }else if (CommonService.validateEmpty(data.password, 'Oops!', 'Please enter password') === false) {
        return false;
      }else if (CommonService.validateEmpty(data.conform_password, 'Oops!', 'Please enter conform password') === false) {
        return false;
      }else if (CommonService.compairePassword(data.password,data.conform_password, 'Oops!', "Password doesn't match") === false) {
        return false;
      }
    }

  })
